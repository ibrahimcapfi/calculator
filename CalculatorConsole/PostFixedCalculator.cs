﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public class PostFixedCalculator
    {
        private Stack<double> _stack = new Stack<double>();

        public void Add()
        {
            double d1 = _stack.Pop();
            double d2 = _stack.Pop();

            _stack.Push(d1 + d2);
        }

        public void Sub()
        {
            double d1 = _stack.Pop();
            double d2 = _stack.Pop();

            _stack.Push(d1 - d2);
        }

        public void Mul()
        {
            double d1 = _stack.Pop();
            double d2 = _stack.Pop();

            _stack.Push(d1 * d2);
        }

        public void Div()
        {
            double d1 = _stack.Pop();
            double d2 = _stack.Pop();

            if (d2 == 0.00)
                throw new ArithmeticException("could not devise by Zéro");

            _stack.Push(d1 / d2);
        }

        public double Pop()
        {
            return _stack.Pop();
        }

        public double Peek()
        {
            return _stack.Peek();
        }

        public void Push(double number)
        {
            _stack.Push(number);
        }
    }
}
