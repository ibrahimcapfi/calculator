﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public enum enumTypeToken
    {
        Number,
        Add,
        Sub,
        Multiply,
        Divide,
        LParent,
        RParent,
        Symbol,
        Help,
        Error,
        Quit
    }

    public class Token
    {
        enumTypeToken _typeTocken;
        string _value;

        public Token(enumTypeToken typeToken, string value)
        {
            _typeTocken = typeToken;
            _value = value;
        }

        public enumTypeToken TypeToken { get { return _typeTocken; } }

        public string Value { get { return _value; } }
    }
}
