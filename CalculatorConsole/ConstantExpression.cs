﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public class ConstantExpression : Expression
    {
        public ConstantExpression(double number)
        {
            Value = number;
        }

        public override double Evaluate()
        {
            return Value;
        }

        private double Value { get; set; }
    }
}
