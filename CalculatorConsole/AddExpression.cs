﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public class AddExpression : BinaryExpression
    {
        public AddExpression(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public override double Evaluate()
        {
            
            return Left.Evaluate() + Right.Evaluate() ;
        }
    }
}
