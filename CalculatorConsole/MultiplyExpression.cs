﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public class MultiplyExpression : BinaryExpression
    {
        public MultiplyExpression(Expression left, Expression right)
        {
            Left = left;
            Right = right;
            if(Left == null || Right == null)
            {
                throw new InvalidOperationException();
            }
        }

        public override double Evaluate()
        {
            if (Left == null || Right == null)
            {
                throw new InvalidOperationException();
            }

            return Left.Evaluate() * Right.Evaluate();
        }
    }
}
