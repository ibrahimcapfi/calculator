﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public class DivideExpression : BinaryExpression
    {

        public DivideExpression(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public override double Evaluate()
        {
            if(Right.Evaluate() == 0.00)
            {
                throw new ArithmeticException("could not divide by zero");
            }

            return Left.Evaluate() / Right.Evaluate();
        }
    }
}
