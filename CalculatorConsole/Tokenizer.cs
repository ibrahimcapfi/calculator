﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorConsole
{
    public class Tokenizer
    {
        StringReader _reader;

        public List<Token> Parse(string expression)
        {
            List<Token> tokens = new List<Token>();
            _reader = new StringReader(expression);

            while (_reader.Peek() != -1)
            {
                char c = (char)_reader.Peek();
                if (char.IsWhiteSpace(c))
                {
                    _reader.Read();
                    continue;
                }

                if(char.IsDigit(c))
                {
                    var number = ParseNumber();
                    tokens.Add(new Token(enumTypeToken.Number, number));
                }
                else if(char.IsLetter(c))
                {
                    var command = ParseReader(c);
                    if (command == "HELP")
                    {
                        tokens.Add(new Token(enumTypeToken.Help, command));
                    }
                    else if (command == "QUIT")
                    {
                        tokens.Add(new Token(enumTypeToken.Quit, command));
                    }
                }
                else if(c == '+')
                {
                    tokens.Add(new Token(enumTypeToken.Add, c.ToString()));
                    _reader.Read();
                }
                else if (c == '-')
                {
                    tokens.Add(new Token(enumTypeToken.Sub, c.ToString()));
                    _reader.Read();
                }
                else if (c == '*')
                {
                    tokens.Add(new Token(enumTypeToken.Multiply, c.ToString()));
                    _reader.Read();
                }
                else if (c == '/')
                {
                    tokens.Add(new Token(enumTypeToken.Divide, c.ToString()));
                    _reader.Read();
                }
                else if (c == '(')
                {
                    tokens.Add(new Token(enumTypeToken.LParent, c.ToString()));
                    _reader.Read();
                }
                else if (c == ')')
                {
                    tokens.Add(new Token(enumTypeToken.RParent, c.ToString()));
                    _reader.Read();
                }
                else
                {
                    throw new Exception(string.Format("Carac not Correct {0}", c));
                }


            }

            return tokens;
        }

        private string ParseReader(char c)
        {
            StringBuilder sb = new StringBuilder();
            while (char.IsLetter((char)_reader.Peek()))
            {
                sb.Append((char)_reader.Read());
            }

            switch (sb.ToString().ToUpper())
            {
                case "HELP":
                    return sb.ToString().ToUpper();
                case "QUIT":
                    return sb.ToString().ToUpper();                    
                default:
                    throw new Exception("Command not correct");
            }
        }

        private string ParseNumber()
        {
            StringBuilder sb = new StringBuilder();
            while(char.IsDigit((char)_reader.Peek()) || (char)_reader.Peek()=='.')
            {
                if ((char)_reader.Peek() == '.' && sb.ToString().Contains('.'))
                {
                    throw new Exception("Number format could not contain more than one point");
                }
                sb.Append((char)_reader.Read());
            }

            return sb.ToString();
        }
    }
}
