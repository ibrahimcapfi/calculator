﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// Recursive Descent Parser
// analyseur syntaxical
// analyseur lexical 

namespace CalculatorConsole
{
    public class Parser
    {
        public Expression Parse(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
            {
                throw new ArgumentException(); 
            }

            var tokens = ParseExpressionToListToken(expression);
            int index = 0;

            var exp = ParseExpression(tokens, ref index);
            //List<string> tokens = GetPostFixExpression(expression);

            //var x = ParseExpression(null, tokens, ref index);

            //var y = x.Evaluate();

            return exp;
        }

        public Expression ParseExpression(List<Token> tokens, ref int index)
        {
            Expression exp = null;
            if(index <= tokens.Count())
            {
                Token token = tokens[index];
                switch (token.TypeToken)
                {
                    case enumTypeToken.Number:
                        double d;
                        if(double.TryParse(token.Value, out d))
                        {
                            return new ConstantExpression(d);
                        }
                        else
                        {
                            throw new Exception("Error when converting string to double");
                        }
                    case enumTypeToken.Add:
                        break;
                    case enumTypeToken.Sub:
                        break;
                    case enumTypeToken.Multiply:
                        break;
                    case enumTypeToken.Divide:
                        break;
                    case enumTypeToken.LParent:
                        index++;
                        return ParseExpression(tokens, ref index);
                    case enumTypeToken.RParent:
                        break;
                    case enumTypeToken.Symbol:
                        break;
                    case enumTypeToken.Help:
                        break;
                    case enumTypeToken.Error:
                        break;
                    case enumTypeToken.Quit:
                        break;
                    default:
                        break;
                }
            }
            return exp;
        }

        public Expression ParseExpression(SubExpression data, List<string> tokens, ref int index)
        {
            if(index >= tokens.Count())
            {
                return data != null ? data.Left : null;
            }

            Expression left;
            if (data == null)
            {
                left = ParseSubExpression(tokens, ref index);
            }
            else
            {
                left = data.Left;
            }
            
            if (index == tokens.Count())
            {
                return left;
            }

            Expression right = null;
            string token = tokens[index];
            if(IsOperator(token))
            {
                index++;
                right = ParseSubExpression(tokens, ref index);

                left = GetDataExpression(left, right, token);
                return left;
            }
            else // if is command
            {
                right = ParseSubExpression(tokens, ref index);
                token = tokens[index];
                if (IsOperator(token))
                {
                    index++;
                    left = GetDataExpression(left, right, token);
                }
                //else
                //{
                //    var e = right; 

                //}
                return ParseExpression(new SubExpression(left, right), tokens, ref index);
            }
        }

        private Expression ParseSubExpression(List<string> tokens, ref int index)
        {
            double d;
            string token = tokens[index];
            if (!IsOperator(token) && double.TryParse(token, out d))
            {
                index++;
                return new ConstantExpression(d);
            }
            else
            {
                return null;
            }
            
        }

        private List<Token> ParseExpressionToListToken(string expression)
        {
            List<Token> tokens = new Tokenizer().Parse(expression);

            return tokens;
        }

        public Expression ParseExpression1(string expression)
        {
            Expression e = null;
            if (string.IsNullOrWhiteSpace(expression))
            {
                return e;
            }

            List<Expression> l = new List<Expression>();

            foreach (string item in GetPostFixExpression(expression))
            {
                double d;
                if (double.TryParse(item, out d))
                {
                    l.Add(new ConstantExpression(d));
                    //stack.Push(new ConstantExpression(d));
                }
                else if (IsOperator(item))
                {
                    Expression right = l.Last(); //stack.Pop();
                    l.Remove(right);
                    Expression left = l.Last(); //stack.Pop();
                    l.Remove(left);
                    l.Add(GetDataExpression(left, right, item));
                }
            }

            if(l.Count() == 1)
            {
                e = l.Last();
            }

            return e;
        }

        public static List<string> GetListExpression(string expression)
        {
            List<string> tokens = new List<string>();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < expression.Length; i++)
            {
                char c = expression[i];
                switch (c)
                {
                    case ' ':
                        if (sb.Length == 0)
                        {
                            continue;
                        }
                        sb.Append(c);
                        break;
                    case '(':
                    case ')':
                        if(sb.Length > 0)
                        {
                            tokens.Add(sb.ToString());
                            sb.Clear();
                        }
                        tokens.Add(c.ToString());
                        break;
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                        if (sb.Length != 0)
                        {
                            char prev = sb[sb.Length - 1];
                            if (c == prev) // + || - || * || / 
                            {
                                sb.Remove(sb.Length - 1, 1); 
                                tokens.Add(sb.ToString().Trim());
                                sb.Clear();
                                tokens.Add(c.ToString());
                                break;
                            }
                        }
                        sb.Append(c);
                        break;
                    default:
                        sb.Append(c);
                        break;
                }                
            }

            if (sb.Length > 0)
            {
                tokens.Add(sb.ToString());
            }

            return tokens;
        }

        public double? ParseToDouble(string expression)
        {
            double? result = null;
            if (string.IsNullOrWhiteSpace(expression))
            {
                return result;
            }

            Stack<Expression> stack = new Stack<Expression>();

            foreach (string item in GetPostFixExpression(expression))
            {
                double d;
                if (double.TryParse(item, out d))
                {
                    stack.Push(new ConstantExpression(d));
                }
                else if (IsOperator(item))
                {
                    Expression right = stack.Pop();
                    Expression left = stack.Pop();
                    stack.Push(GetDataExpression(left, right, item));
                }
            }

            if(stack.Count() == 1)
            {
                result = stack.Pop().Evaluate();
            }
            return result;

            //string[] tockensAdd;
            //string[] tockensSub;


            //tockensAdd = expression.Replace(" ", "").Split('-');
            //foreach (var itemAdd in tockensAdd)
            //{
            //    tockensSub = itemAdd.Split('+');
            //    exp = tockensSub.Aggregate(exp, (left, right) =>
            //        new AddExpression(left, new ConstantExpression(Convert.ToDouble(right))));
            //}

            //exp = tockensAdd.Aggregate(exp, (left, right) =>
            //        new SubExpression(left, new ConstantExpression(Convert.ToDouble(right))));

            //if (expression.Contains("-"))
            //{
            //    tockens = expression.Replace(" ", "").Split('-');

            //    exp = tockens.Aggregate(exp, (left, right) =>
            //            new SubExpression(left, new ConstantExpression(Convert.ToDouble(right))));
            //}

            //for (int i = 1; i < tockens.Length; i++)
            //{
            //    exp = new AddExpression(exp, new ConstantExpression(Convert.ToDouble(tockens[i])));
            //}


        }

        public static List<string> GetPostFixExpression(string expression)
        {
            List<string> list = new List<string>();
            Stack<string> stack = new Stack<string>();
            foreach (string item in expression.Split(' '))
            {
                if (!IsOperator(item))
                {
                    switch (item)
                    {
                        case "(":
                            stack.Push(item);
                            break;
                        case ")":
                            var pop = stack.Pop();
                            while (pop != "(")
                            {
                                list.Add(pop);
                                pop = stack.Pop();
                            }
                            break;
                        default:
                            list.Add(item);
                            break;
                    }
                }
                else
                {
                    while (stack.Count() > 0 && Priority(item) <= Priority(stack.Peek()))
                    {
                        list.Add(stack.Pop());
                    }
                    stack.Push(item);
                }
            }
            while (stack.Count() > 0)
            {
                list.Add(stack.Pop());
            }
            return list;
        }

        public Expression GetDataExpression(Expression left, Expression right, string op)
        {
            Expression exp = null;
            switch (op)
            {
                case "+":
                    exp = new AddExpression(left, right);
                    break;
                case "-":
                    exp = new SubExpression(left, right);
                    break;
                case "*":
                    exp = new MultiplyExpression(left, right);
                    break;
                case "/":
                    exp = new DivideExpression(left, right);
                    break;
            }
            return exp;
        }

        public static int Priority(string op)
        {
            if (op == "*" || op == "/")
            {
                return 2;
            }
            else if (op == "+" || op == "-")
            {
                return 1;
            }
            return 0;
        }

        public static bool IsOperator(string s)
        {
            if (s == "+" || s == "-" || s == "*" || s == "/")
            {
                return true;
            }
            else
                return false;
        }

        public static bool IsOperator(enumTypeToken token)
        {
            if (token == enumTypeToken.Add || token == enumTypeToken.Sub || token == enumTypeToken.Multiply || token == enumTypeToken.Divide)
            {
                return true;
            }
            else
                return false;
        }
    }
}
