﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestConstanteExpression
    {
        Expression _constanteExpression1 = new ConstantExpression(2);
        Expression _constanteExpression2 = new ConstantExpression(45);
        //Expression _constanteExpression3 = new ConstanteExpression("5e");

        [TestMethod]
        public void should_return_2()
        {
            Assert.AreEqual(2, _constanteExpression1.Evaluate());
        }

        [TestMethod]
        public void should_return_45()
        {
            Assert.AreEqual(45, _constanteExpression2.Evaluate());
        }

        //[TestMethod]
        //[ExpectedException(typeof(System.FormatException))]
        //public void should_return_format_exception()
        //{
        //    Assert.AreEqual(45, _constanteExpression3.Evaluate());
        //}
    }
}
