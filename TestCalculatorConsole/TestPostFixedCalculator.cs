﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestPostFixedCalculator
    {
        PostFixedCalculator _postFixedCalculator = new PostFixedCalculator();


        [TestMethod]
        public void should_return_3_when_adding_1_and_2()
        {
            _postFixedCalculator.Push(1);
            _postFixedCalculator.Push(2);
            _postFixedCalculator.Add();
            Assert.AreEqual(3, _postFixedCalculator.Peek());
        }

        [TestMethod]
        public void should_return_1_when_substring_2_to_1()
        {
            _postFixedCalculator.Push(1);
            _postFixedCalculator.Push(2);
            _postFixedCalculator.Sub();
            Assert.AreEqual(1, _postFixedCalculator.Peek());
        }

        [TestMethod]
        public void should_return_2_when_multipling_3_to_5()
        {
            _postFixedCalculator.Push(3);
            _postFixedCalculator.Push(5);
            _postFixedCalculator.Mul();
            Assert.AreEqual(15, _postFixedCalculator.Peek());
        }

        [TestMethod]
        public void should_return_3_when_divising_9_by_3()
        {
            _postFixedCalculator.Push(3);
            _postFixedCalculator.Push(9);
            _postFixedCalculator.Div();
            Assert.AreEqual(3, _postFixedCalculator.Peek());
        }

        [TestMethod, ExpectedException(typeof(System.ArithmeticException))]
        public void should_return_ArithmeticException_when_divising_by_0()
        {
            _postFixedCalculator.Push(0);
            _postFixedCalculator.Push(3);
            _postFixedCalculator.Div();
        }
    }
}
