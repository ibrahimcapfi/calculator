﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestMultiplyExpression
    {

        [TestMethod]
        public void should_return_16_when_multipling_4_by_4()
        {
            Expression left = new ConstantExpression(4);
            Expression right = new ConstantExpression(4);
            MultiplyExpression exp = new MultiplyExpression(left, right);
            Assert.AreEqual(16, exp.Evaluate());
        }
    }
}
