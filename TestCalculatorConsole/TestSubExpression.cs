﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestSubExpression
    {
        [TestMethod]
        public void should_return_3_when_subing_9_to_6()
        {
            Expression left = new ConstantExpression(9);
            Expression right = new ConstantExpression(6);
            SubExpression exp = new SubExpression(left, right);
            Assert.AreEqual(3, exp.Evaluate());
        }
    }
}
