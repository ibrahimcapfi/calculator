﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestDivideExpression
    {

        [TestMethod]
        public void should_return_3_when_dividing_15_by_5()
        {
            Expression left = new ConstantExpression(15);
            Expression right = new ConstantExpression(5);
            DivideExpression exp = new DivideExpression(left, right);
            Assert.AreEqual(3, exp.Evaluate());
        }

        [TestMethod, ExpectedException(typeof(ArithmeticException))]
        public void should_thronw_exception_when_dividing_by_0()
        {
            Expression left = new ConstantExpression(15);
            Expression right = new ConstantExpression(0);
            DivideExpression exp = new DivideExpression(left, right);
            Assert.AreEqual(0, exp.Evaluate());
        }
    }
}
