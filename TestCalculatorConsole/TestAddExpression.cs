﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestAddExpression
    {

        [TestMethod]
        public void should_return_5_when_adding_2_to_3()
        {
            Expression left = new ConstantExpression(2);
            Expression right = new ConstantExpression(3);
            AddExpression exp = new AddExpression(left, right);
            Assert.AreEqual(5, exp.Evaluate());
        }
    }
}
