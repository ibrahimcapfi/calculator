﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestInfixedCalculator
    {
        InfixedCalculator _infexedCalculator = new InfixedCalculator();

        
        

        [TestMethod]
        public void should_return_1_when_operator_is_plus()
        {
            Assert.AreEqual(1, InfixedCalculator.Priority("+"));
        }

        [TestMethod]
        public void should_return_2_when_operator_is_division()
        {
            Assert.AreEqual(2, InfixedCalculator.Priority("/"));
        }

        [TestMethod]
        public void should_return_true_when_is_an_operator()
        {
            Assert.AreEqual(true, InfixedCalculator.IsOperator("/"));
        }
    }
}
