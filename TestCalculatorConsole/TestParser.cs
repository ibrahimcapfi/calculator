﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestParser
    {
        Parser _parser = new Parser();

        //[TestMethod]
        //public void should_return_null_when_expression_is_empty()
        //{
        //    Assert.AreEqual(null, _parser.ParseToDouble(" "));
        //}

        


        //[TestMethod]
        //public void should_return_15_when_expression_is_5_plus_8_plus_2()
        //{
        //    Assert.AreEqual(15, _parser.ParseToDouble("5 + 8 + 2"));
        //}

        //[TestMethod]
        //public void should_return_11_when_expression_is_5_plus_8_sub_2()
        //{
        //    Assert.AreEqual(11, _parser.ParseToDouble("5 + 8 - 2"));
        //}

        //[TestMethod]
        //public void should_return_141_when_expression_is_5_mul_8_add_7_mul_3()
        //{
        //    Assert.AreEqual(141, _parser.ParseToDouble("( 5 * 8 + 7 ) * 3"));
        //}

        //[TestMethod]
        //public void should_return_10_125_when_expression_is_long()
        //{
        //    Assert.AreEqual(10.125, _parser.Parse("( 5 * 8 + 7 - 4 * 5 / ( 5 + 3 ) ) * 3"));
        //}

        //[TestMethod]
        //public void should_return_exp_when_expression_is_5_mul_8_add_7_mul_3()
        //{
        //    Assert.AreEqual(141, _parser.Parse("( 5 * 8 + 7 ) * 3"));
        //}

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void should_return_null_when_expression_is_empty()
        {
            _parser.Parse(" ");
        }

        [TestMethod]
        public void should_return_5_when_expression_is_5()
        {
            Assert.AreEqual(5, _parser.Parse("5").Evaluate());
        }

        [TestMethod]
        public void should_return_13_when_expression_is_5_plus_8()
        {
            Assert.AreEqual(13, _parser.Parse("5 + 8").Evaluate());
        }
    }
}
