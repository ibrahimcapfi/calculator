﻿using CalculatorConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculatorConsole
{
    [TestClass]
    public class TestTokenizer
    {
        Tokenizer _tokenizer = new Tokenizer();

        [TestMethod]
        public void _should_return_5_when_expression_is_5()
        {
            var x = _tokenizer.Parse("5").FirstOrDefault();
            Assert.AreEqual("5", x.Value);
        }

        [TestMethod]
        public void _should_return_3_point_5_when_expression_is_3_point_5()
        {
            var x = _tokenizer.Parse("3.5").FirstOrDefault();
            Assert.AreEqual("3.5", x.Value);
        }

        [TestMethod, ExpectedException(typeof(Exception))]
        public void _should_return_exception_when_expression_containt_more_than_one_point()
        {
            var x = _tokenizer.Parse("3.5.4").FirstOrDefault();
        }

        [TestMethod]
        public void _should_return_add_when_expression_is_add()
        {
            var x = _tokenizer.Parse("+").FirstOrDefault();
            Assert.AreEqual("+", x.Value);
        }

        [TestMethod]
        public void _should_return_sub_when_expression_is_sub()
        {
            var x = _tokenizer.Parse("-").FirstOrDefault();
            Assert.AreEqual("-", x.Value);
        }

        [TestMethod]
        public void _should_return_mul_when_expression_is_mul()
        {
            var x = _tokenizer.Parse("*").FirstOrDefault();
            Assert.AreEqual("*", x.Value);
        }

        [TestMethod]
        public void _should_return_divide_when_expression_is_div()
        {
            var x = _tokenizer.Parse("/").FirstOrDefault();
            Assert.AreEqual("/", x.Value);
        }

        [TestMethod]
        public void _should_return_RParent_when_expression_is_rparent()
        {
            var x = _tokenizer.Parse(")").FirstOrDefault();
            Assert.AreEqual(")", x.Value);
        }

        [TestMethod]
        public void _should_return_LParent_when_expression_is_Lparent()
        {
            var x = _tokenizer.Parse("(").FirstOrDefault();
            Assert.AreEqual("(", x.Value);
        }

        [TestMethod, ExpectedException(typeof(Exception))]
        public void _should_return_exception_when_expression_is_not_correct_carac()
        {
            var x = _tokenizer.Parse("%").FirstOrDefault();
        }

        [TestMethod]
        public void _should_return_9_when_expression_is_correct_and_contains_5_tokens()
        {
            var x = _tokenizer.Parse("(5 * 8) + 3 * 7");
            Assert.AreEqual(9, x.Count());
        }

        [TestMethod]
        public void _should_return_help_when_expression_contains_help()
        {
            var x = _tokenizer.Parse("(5 * help) + 3 * 7");
            Assert.AreEqual("HELP", x[3].Value);
        }

        [TestMethod]
        public void _should_return_quit_when_expression_contains_quit()
        {
            var x = _tokenizer.Parse("(5 * qUit) + 3 * 7");
            Assert.AreEqual("QUIT", x[3].Value);
        }
    }
}
